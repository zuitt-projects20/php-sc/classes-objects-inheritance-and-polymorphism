<?php

class Person {
	public function __construct($fName, $mName, $lName){
		$this->fName = $fName;
		$this->mName = $mName;
		$this->lName = $lName;
	}

	function printName(){
		return "Your full name is $this->fName $this->mName $this->lName";
	}

};

class Developer extends Person{
	function printName(){
		return "Your name is $this->fName $this->lName and you are a developer";
	}
};

class Engineer extends Person{
	function printName(){
		return "You are an engineer named $this->fName $this->lName";
	}
}

$personOne = new Person("Mang", "Jose", "Thomas");
$developerOne = new Developer("Papeng", "JR", "Guerrero");
$engineerOne = new Engineer("Ewan", "Ko", "Sayo");

?>