<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S3: Activity</title>
</head>
<body>
	<h1>S3: Activity</h1>
	<h3>Person</h3>
	<p><?= $personOne->printName(); ?></p>
	<h3>Developer</h3>
	<p><?= $developerOne->printName(); ?></p>
	<h3>Engineer</h3>
	<p><?= $engineerOne->printName(); ?></p>
</body>
</html>